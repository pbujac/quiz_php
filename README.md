Quiz 
===========
A quiz website - dashboard
 -  CRUD users
 - CRUD quiz + questions + answers

## Built With

* [PHP](http://php.net) - The programming language used
* [Symfony + Migrations](https://symfony.com) -  The web framework used
* Docker
* [Bootstrap](http://www.bootstrap.com) - UI framework


## Run project
Make sure you have installed PHP > 5.6.32, Composer, Doceker and Symfony


 Open project and run in terminal:
```
docker-compose build
docker-compose up
```

## Screenshots

### Login page
![Alt text](screenshots/img1.png?raw=true "Articles Page")

### Users page
![Alt text](screenshots/img2.png?raw=true "Articles Page")
### Add user page
![Alt text](screenshots/img3.png?raw=true "Articles Page")
### Edit quiz page
![Alt text](screenshots/img4.png?raw=true "Articles Page")
### Add question page
![Alt text](screenshots/img5.png?raw=true "Articles Page")